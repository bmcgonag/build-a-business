---
description: Ideas for Building a Business on Open Source solutions
---

# Concept - Build a Business on Open Source Software

### Ideas / Workflow

* What is really needed for a business to get off the ground?
  * Business Plan
    * What do we do?
      * Consulting
      * IT Contracting
      * MSP - Managed Service Provider
      * Software Service Provider (Cloud / Hosted)
      * Desktop Support?
      * Server Support?
      * Mobile Support?
    * How do we make money?
      * Initial Sign On cost?
      * Subcription / Monthly Maintenance
        * Contracts?
    * What kind of equipment do we use?
      * Self / Own Hardware / Servers?
      * VPS / VMs / Cloud Services?
    * Advertising / Marketing
      * Domain Name
      * Domain Management
      * DNS
      * Website (see Support Portal below as well)
    * Internal IT Infrastructure
      * Network Architecture
      * VPN(s)
      * Firewalls
      * Routing
  * How do I contribute back to the Projects I choose to use to ensure their logevity?
  * Internal Documentation (for employees)
  * Support Portal
    * Ticket / Support Request system
    * Client facing documentation.
    * PBX System?
  * Internal Communications
    * Caht
    * Video / Audio
    * Email

**How can we get the community involved with this series**?

* Polls after each episode?
  * Ask for their choice on software to setup for our business?
  * Ask for a Comparison choice (something to compare for future poll of which one to use?)
  * Setup demo sites for viewers to log in and test out with us?
    * Set some long time subscribers as admins / moderators?

