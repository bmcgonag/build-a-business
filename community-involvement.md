# Community Involvement

I think we have several places / ways to get the viewers / community involved in these videos with us.&#x20;

* First line out in a video what we'll be doing - building a Business on Open Source software / Services
  * Ask the community what business / businesses they would be most interested in seeing us try to build out with 3 or 4 options maybe?
    * Options
* Next, each week, think through what we need in order to build our business and offer a couple or 3 software options in a feature comparison based on what's available
  * Maybe you do an install and review of one, and I do an install and review of one, link to each others' videos, and then put up a poll on each channel about which one to go with?
*

### Side Thought
I also want to be very upfront about what we will do.  I would like to solicit thoughts on whatever business(es) we choose to create.  Let the audience know our knowledge level about that business, and solicit their input as potential pros in that area.  But also make sure we are clear that while we may get a ton of feedback, we do have to make a decision on way or another in order to continue forward.  Admit that we'll likely make mistakes along the way (just like in a real life business).   Basically try to keep those giving input from feeling like we disregarded their input, or ignored them.
