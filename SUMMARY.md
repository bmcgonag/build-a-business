# Table of contents

* [Concept - Build a Business on Open Source Software](README.md)
  * [Community Involvement](https://app.gitbook.com/s/UAu2GhMW7RxhQICmkuiV/\~/changes/pz4S3hSVy10UJYcDxtte/community-involvement)
  * [Schedule for Releases](https://app.gitbook.com/s/UAu2GhMW7RxhQICmkuiV/\~/changes/pz4S3hSVy10UJYcDxtte/series-release-schedule)
* [Community Involvement](community-involvement.md)
* [Series Release Schedule](series-release-schedule.md)
