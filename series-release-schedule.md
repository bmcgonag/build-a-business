# Series Release Schedule

I know you are super busy, as am I, so I don't want to make this feel like a high pressure deal.&#x20;

* Do we want to keep doing our own videos in between these?
  * If so, what's a good cadence for the series?
    * every other week?
    * once a month?
  * I'm pretty open here.
* Or do we feel like the material will be broad enough that we would use this series to push our weekly output?

I'm just throwing out thoughts and questions, and want to have a good discussion around this.  I'm excited about it, but also want to make sure we give ourselves time to live, as well as time to make the best series we can.

Questions and Thoughts?
* Should we start a new combined channel?
* Would it be better to each take a product for each thing we are adding to our business each episode, do a video on install, setup, and some use cases, then do a combined video where we discuss the merits, compare them for 10 - 15 minutes, then let the audience choose which one we'll implement? (unless we find one to be simply not up to the task and just choose the clear winner.)
